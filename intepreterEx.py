import sys

displayCount = 1
displayString = \
f"""
\033c 
  i i i i i i i       ooooo    o        ooooooo   ooooo   ooooo     
  I I I I I I I      8     8   8           8     8     o  8    8    
  I  \ `+' /  I      8     8   8           8     8        8    8    
   \  `-+-'  /       8 8 8 8   8           8      ooooo   8oooo
    `-__|__-'        8         8           8           8  8
        |            8         8           8     o     8  8
  ------+------      8          oooooo  ooo8ooo   ooooo   8         team107

GNU CLISP 2.49.92 (2018-02-18) <http://clisp.org/>

Type :h and hit Enter for context help.
     (exit) or (quit) and hit Enter to exit the intepreter.

[{displayCount}]> """


a = 0
while len(sys.argv) == 1 and a<3:

    input01 = input(displayString).strip()

    displayCount += 1

    displayString = f"[{displayCount}]> "
    
    a+=1

